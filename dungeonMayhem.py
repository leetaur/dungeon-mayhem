#!/usr/bin/python3

import pymysql
import passWdLookup
from getpass import getpass

def createNewUser(username, password):
    (dbUsername, dbPassword) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",dbUsername,dbPassword,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = 'insert into players (name, password) values ("' + username + '", "' +  password + '")'
    #print(sql)
    cursor.execute(sql)
    db.commit()
    cursor.close()

def checkIfUserExists(username):
    (dbUsername, dbPassword) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",dbUsername,dbPassword,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = "select name from players where name = '" + username + "'"
    cursor.execute(sql)

    # Fetch a single row using fetchone() method.
    results = cursor.fetchone()

    if results == None:
        return False
    else:
        return True

def checkPassword(username, password):
    (dbUsername, dbPassword) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",dbUsername,dbPassword,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = "select password from players where name = '" + username + "'"
    cursor.execute(sql)

    # Fetch a single row using fetchone() method.
    results = cursor.fetchone()
    # disconnect from server
    db.close()

    if results == None:
        print("User does not exist")
        return False
    else:
        passwordReturned = results[0]
        if passwordReturned == password:
            return True
        else: 
            return False


def getRoomDetails(currentLocation):
    (username, password) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",username,password,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = "select roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription from rooms where roomName = '" + currentLocation + "'"
    cursor.execute(sql)

    # Fetch a single row using fetchone() method.
    results = cursor.fetchone()
    roomName = results[0]
    northExit = results[1]
    southExit = results[2]
    eastExit = results[3]
    westExit = results[4]
    upExit = results[5]
    downExit = results[6]
    roomDescription = results[7]

    # disconnect from server
    db.close()

    return northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription


def loginUser():
    loginAccepted = False
    numberOfFailedLogins = 0

    while loginAccepted == False:
        userName = input("Username: ")
        password = getpass()
        userExists = checkIfUserExists(userName)
        if userExists == True:
            loginAccepted = checkPassword(userName, password)
            if loginAccepted == True:
                return userName
            else:
                print("Invalid password")
                numberOfFailedLogins += 1
                if numberOfFailedLogins > 3:
                    print("Be off with you!!")
                    exit()
        else:
            print("Silly, user does not exists")
            return ''

def loginNewUser():
    userNameAccepted = False
    while userNameAccepted == False:
        userName = input("Username: ")
        userExists = checkIfUserExists(userName)
        if userExists == True:
            print("User already exists. Choose another name")
        else:
            password = getpass()
            userNameAccepted = True
            createNewUser(userName, password)
    return userName

def startGame():
    loggedInUser = ''
 
    #########################################
    # FIRST, HANDLE LOGIN
    ######################################### 
    loginChoice = input("Choose (L)ogin or (N)ew User: ")
    if loginChoice == 'L':
        loggedInUser = loginUser()
    elif loginChoice == 'N':
        loggedInUser = loginNewUser()
    else:
        print("Fine, do nothing")
        exit()

    exit()
    name = input("What is your name, hero?\n")
    currentLocation = "entrance"

    # Get room details
    (northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription) = getRoomDetails(currentLocation)
    
    print(roomDescription)
    exitToCheck = ""

    userInput = 'r'
    while userInput != 'q':
        userInput = input(name + ", tell me what to do?  ")
        if userInput == 'n':
            exitToCheck = northExit
        if userInput == 's':
            exitToCheck = southExit
        if userInput == 'e':
            exitToCheck = eastExit
        if userInput == 'w':
            exitToCheck = westExit
        if userInput == 'u':
            exitToCheck = upExit
        if userInput == 'd':
            exitToCheck = downExit
        if exitToCheck == "NO-EXIT":
            print("\n\nThere is no escape in that direction.")
        else:
            # Move
            if userInput != 'q':
                (northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription) = getRoomDetails(exitToCheck)
                print("\n\n" + roomDescription)
            else:
                print("Farewell, ", name)

    print("Good-bye")

# Main program

startGame();

