#!/usr/bin/python3

import pymysql
import passWdLookup

def getRoomDetails(currentLocation):
    (username, password) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",username,password,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = "select roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription from rooms where roomName = '" + currentLocation + "'"
    cursor.execute(sql)

    # Fetch a single row using fetchone() method.
    results = cursor.fetchone()
    roomName = results[0]
    northExit = results[1]
    southExit = results[2]
    eastExit = results[3]
    westExit = results[4]
    upExit = results[5]
    downExit = results[6]
    roomDescription = results[7]

    # disconnect from server
    db.close()

    return northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription

def startGame():
    name = input("What is your name, hero?\n")
    currentLocation = "entrance"

    # Get room details
    (northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription) = getRoomDetails(currentLocation)
    
    print(roomDescription)
    exitToCheck = ""

    userInput = 'r'
    while userInput != 'q':
        userInput = input(name + ", tell me what to do?  ")
        if userInput == 'n':
            exitToCheck = northExit
        if userInput == 's':
            exitToCheck = southExit
        if userInput == 'e':
            exitToCheck = eastExit
        if userInput == 'w':
            exitToCheck = westExit
        if userInput == 'u':
            exitToCheck = upExit
        if userInput == 'd':
            exitToCheck = downExit
        if exitToCheck == "NO-EXIT":
            print("\n\nThere is no escape in that direction.")
        else:
            # Move
            if userInput != 'q':
                (northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription) = getRoomDetails(exitToCheck)
                print("\n\n" + roomDescription)
            else:
                print("Farewell, ", name)

    print("Good-bye")

# Main program

startGame();

