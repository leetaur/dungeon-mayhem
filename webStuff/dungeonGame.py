#!/usr/bin/python3 
# Importing the 'cgi' module 
import cgi 
import pymysql
import json
import passWdLookup
import os
from http import cookies

############################################
# SQL
############################################

def getRoomDetails(currentLocation):
    (username, password) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",username,password,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = "select roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic from rooms where roomName = '" + currentLocation + "'"
    cursor.execute(sql)

    # Fetch a single row using fetchone() method.
    results = cursor.fetchone()
    roomName = results[0]
    northExit = results[1]
    southExit = results[2]
    eastExit = results[3]
    westExit = results[4]
    upExit = results[5]
    downExit = results[6]
    roomDescription = results[7]
    roomGraphic = results[8]

    # disconnect from server
    db.close()

    return roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic



############################################
# INPUT PARMETERS
############################################

def getWebParams():
    roomName = "entrance"
    direction = ""
    form = cgi.FieldStorage() 
    print(form)

    for item in form.list:
        if item.name and item.value:
            if item.name == 'room':
                roomName = item.value
            if item.name == 'direction':
                direction = item.value

    # Cookies
    if "HTTP_COOKIE" in os.environ:
        print(os.environ["HTTP_COOKIE"])
    else:
        print("HTTP_COOKIE not set!")

    #C = cookies.SimpleCookie()
    #C["fig"].value

    return roomName, direction
    

############################################
# HTML
############################################

def printContentInfo():
    print("Content-type: text/html\n")

def printCookieInfo():
    C = cookies.SimpleCookie()
    C["leetaur"] = "lion"
    C["leetaur"]["expires"] = 12 * 30 * 24 * 60 * 60 #  1 year

    print(C)

def printHeaderInfo():
    # present initial HTML
    print("<html><head><title>The Monsters of Deep Rock</title></head>\n\n")
    printStyle()
    print("<body>\n")

    print('<center>' + "\n")
    print("<H1>The Monsters of Deep Rock</H1>\n")
    print('</center>' + "\n")


def printStyle():
    print('<style type="text/css">' + "\n")
    print('  body {' + "\n")
    print('          background-color: #d2b48c;' + "\n")
    print('          margin-left: 20%;' + "\n")
    print('          margin-right: 20%;' + "\n")
    print('          border: 2px dotted black;' + "\n")
    print('          padding: 10px 10px 10px 10px;' + "\n")
    print('          font-family: sans-serif;' + "\n")
    print('  }' + "\n")
    print('</style>' + "\n")

def printRoomDetails(roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit, roomGraphic):
    print("Graphic = ", roomGraphic)
    print("<P>" + roomDescription + "</P>")
    
def printForm(roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit, roomGraphic):
    print('<form method=POST action="http://45.79.218.232/cgi-bin/dungeonGame.py">' + "\n")

    print('<input type="hidden" name="room" value="' + roomName + '"/>' + "\n")

    print('<center>' + "\n")
    print('<input name="direction" type="submit" value="Go North"><BR>'     + "\n")
    print('<input name="direction"  type="submit" value="Go West">    '      + "\n")
    print('<input name="direction"  type="submit" value="Go East"><BR>'      + "\n")
    print('<input name="direction" type="submit" value="Go South"><BR><BR>' + "\n")
    print('<input name="direction"    type="submit" value="Go Up">      <BR>'  + "\n")
    print('<input name="direction" type="submit" value="Go Down">    <BR>'   + "\n")
    print('</center>' + "\n")

    print('</form>' + "\n")

def printClose():
    print("</body></html>")

############################################
# PROGRAM LOGIC
############################################

def moveAndGetNewRoomDetails(direction, roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit, roomGraphic):
    if direction == "Go North":
        if northExit != "NO-EXIT":
            roomName = northExit
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
            #print("new room name = ", roomName)
        else:
            print("<P>You cannot go north here</P>")
    elif direction == "Go South":
        if southExit != "NO-EXIT":
            roomName = southExit
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
            #print("new room name = ", roomName)
        else:
            print("You cannot go south here")
    elif direction == "Go East":
        if eastExit != "NO-EXIT":
            roomName = eastExit
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
            #print("new room name = ", roomName)
        else:
            print("<P>You cannot go east here</P>")
    elif direction == "Go West":
        if westExit != "NO-EXIT":
            roomName = westExit
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
            #print("new room name = ", roomName)
        else:
            print("<P>You cannot go west here</P>")
    elif direction == "Go Up":
        if upExit != "NO-EXIT":
            roomName = upExit
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
            #print("new room name = ", roomName)
        else:
            print("<P>You cannot go up here</P>")
    elif direction == "Go Down":
        if downExit != "NO-EXIT":
            roomName = downExit
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
            #print("new room name = ", roomName)
        else:
            print("<P>You cannot go down here</P>")

    return roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic

############################################
# MAIN PROGRAM
############################################

printCookieInfo()
printContentInfo()
printHeaderInfo()
roomName, direction = getWebParams()
#print("<p>room:", roomName, ", direction: ", direction)


(roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(roomName)
(roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = moveAndGetNewRoomDetails(direction, roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit, roomGraphic)

printRoomDetails(roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit, roomGraphic)
printForm(roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit, roomGraphic)
printClose()

