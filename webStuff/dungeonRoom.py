#!/usr/bin/python3 
# Importing the 'cgi' module 
import cgi 
import pymysql
import json
import passWdLookup

# Functions
def convertRoomDetailsToJson(roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit):
    roomDictionary = {}
    roomDictionaryObject = {}

    roomDictionary["roomName"] = roomName
    roomDictionary["roomDescription"] = roomDescription
    roomDictionary["northExit"] = northExit
    roomDictionary["southExit"] = southExit
    roomDictionary["eastExit"] = eastExit
    roomDictionary["westExit"] = westExit
    roomDictionary["upExit"] = upExit
    roomDictionary["downExit"] = downExit
                                        
    roomDictionaryObject["roomDictionary"] = roomDictionary
    roomJson = json.dumps(roomDictionaryObject, sort_keys=True, indent=4)

    print(roomJson)

def getRoomDetails(currentLocation):
    (username, password) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",username,password,"adventure" )

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = "select roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription from rooms where roomName = '" + currentLocation + "'"
    cursor.execute(sql)

    # Fetch a single row using fetchone() method.
    results = cursor.fetchone()
    roomName = results[0]
    northExit = results[1]
    southExit = results[2]
    eastExit = results[3]
    westExit = results[4]
    upExit = results[5]
    downExit = results[6]
    roomDescription = results[7]

    # disconnect from server
    db.close()

    return roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription


print("Content-type: application/json\r\n\r\n") 
      
form = cgi.FieldStorage() 

for item in form.list:
    if item.name and item.value:
        if item.name == 'room':
            (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription) = getRoomDetails(item.value)
            convertRoomDetailsToJson(roomName, roomDescription, northExit, southExit, eastExit, westExit, upExit, downExit)

