#!/usr/bin/python3

import pymysql
import passWdLookup

def createNewRoom(roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic):
    (username, password) = passWdLookup.sqlCreds()

    # Open database connection
    db = pymysql.connect("localhost",username,password,"adventure")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    sql = 'insert into rooms (roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) values ("' + roomName + '", "' + northExit + '", "' + southExit + '", "' + eastExit + '", "' + westExit + '", "' + upExit + '", "' + downExit + '", "' + roomDescription + '", "' + roomGraphic + '")'
    print(sql)
    cursor.execute(sql)
    db.commit()
    cursor.close()

def startGame():
    roomName = input("Room Name:\n")
    northExit = input("North Exit:\n")
    southExit = input("South Exit:\n")
    eastExit = input("East Exit:\n")
    westExit = input("West Exit:\n")
    upExit = input("Up Exit:\n")
    downExit = input("Down Exit:\n")
    roomDescription = input("Room Description:\n")
    roomGraphic = input("Room Graphic:\n")

    createNewRoom(roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic)

    print("Good-bye")

# Main program

startGame();

