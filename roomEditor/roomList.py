#!/usr/bin/python3

import pymysql
import passWdLookup

def getRooms():
    try:
        (username, password) = passWdLookup.sqlCreds()

        # Open database connection
        db = pymysql.connect("localhost",username,password,"adventure" )

        # prepare a cursor object using cursor() method
        cursor = db.cursor()

        # execute SQL query using execute() method.
        sql = "select roomName from rooms"
        cursor.execute(sql)

        # Fetch all rows
        rows = cursor.fetchall()
        # disconnect from server
        db.close()
        return rows
    except:
        return []

def startGame():

    # Get room details
    rooms = getRooms()
    print("===================")
    for room in rooms:
        print(room[0])
    print("===================")
    print("Good-bye")

# Main program

startGame();

