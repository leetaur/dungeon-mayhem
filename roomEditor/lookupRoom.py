#!/usr/bin/python3

import pymysql
import passWdLookup

def getRoomDetails(currentLocation):
    try:
        (username, password) = passWdLookup.sqlCreds()

        # Open database connection
        db = pymysql.connect("localhost",username,password,"adventure" )

        # prepare a cursor object using cursor() method
        cursor = db.cursor()

        # execute SQL query using execute() method.
        sql = "select id, roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic from rooms where roomName = '" + currentLocation + "'"
        cursor.execute(sql)

        # Fetch a single row using fetchone() method.
        results = cursor.fetchone()
    
        roomId = results[0]
        roomName = results[1]
        northExit = results[2]
        southExit = results[3]
        eastExit = results[4]
        westExit = results[5]
        upExit = results[6]
        downExit = results[7]
        roomDescription = results[8]
        roomGraphic = results[9]

        # disconnect from server
        db.close()

        return roomId, roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic
    except:
        return -1, "", "", "", "", "", "", "", "", ""

def startGame():
    location = input("Room to lookup?\n")

    # Get room details
    (roomId, roomName, northExit, southExit, eastExit, westExit, upExit, downExit, roomDescription, roomGraphic) = getRoomDetails(location)
   
    if roomId > -1:
        print("Room Id = ", roomId)
        print("Room Name = ", roomName)
        print("Room Graphic = ", roomGraphic)
        print("Room Description = ", roomDescription)
        print("North Exit = ", northExit)
        print("South Exit = ", southExit)
        print("East Exit = ", eastExit)
        print("West Exit = ", westExit)
        print("Up Exit = ", upExit)
        print("Down Exit = ", downExit)
    else:
        print("Room not found")

    print("Good-bye")

# Main program

startGame();

